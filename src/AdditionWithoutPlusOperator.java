
public class AdditionWithoutPlusOperator {

    public static void main(String args[]) {
        int a = 10, b = 5;
        while (b != 0) {
            a++;
            b--;
        }
        System.out.println(a);
        a = 10;
        b = 5;
        int temp = b;
        b = a;
        a = temp;
        System.out.println("a is:" + a + " b is:" + b);
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        int c = true?a:b;
        System.out.println("a is:" + a + " b is:" + b);
    }
}
